'use strict';
const sense = require('sense-hat-led');
const exec = require('exec');

let white = [255, 255, 255];
let red = [255, 0, 0];
let green = [0, 255, 0];
let black = [0, 0, 0];

let colors = [
    white,
    red,
    green
];

let colorRed = [
    red, red, red, red, red, red, red, red,
    red, red, red, red, red, red, red, red,
    red, red, red, red, red, red, red, red,
    red, red, red, red, red, red, red, red,
    red, red, red, red, red, red, red, red,
    red, red, red, red, red, red, red, red,
    red, red, red, red, red, red, red, red,
    red, red, red, red, red, red, red, red
];

let colorWhite = [
    white, white, white, white, white, white, white, white,
    white, white, white, white, white, white, white, white,
    white, white, white, white, white, white, white, white,
    white, white, white, white, white, white, white, white,
    white, white, white, white, white, white, white, white,
    white, white, white, white, white, white, white, white,
    white, white, white, white, white, white, white, white,
    white, white, white, white, white, white, white, white
];

let colorGreen = [
    green, green, green, green, green, green, green, green,
    green, green, green, green, green, green, green, green,
    green, green, green, green, green, green, green, green,
    green, green, green, green, green, green, green, green,
    green, green, green, green, green, green, green, green,
    green, green, green, green, green, green, green, green,
    green, green, green, green, green, green, green, green,
    green, green, green, green, green, green, green, green
];

console.log("Starting up bot and setting LEDs to white...");
sense.setPixels(colorWhite);

module.exports = (bot) => {
    let inUse = false;

    bot.hear(/(.*)/gi, (bot) => {
        let text = bot.message.text;
        let lower = text.toLowerCase();

        if (lower === "hello") {
            sense.setPixels(colorGreen);
            return;
        }

        if (lower === "tesla") {
            exec("teslacmd -wFH", (err, out, code) => {
                if (out) {
                    console.log(out);
                }
            });
            return;
        }

        if (inUse) {
            return;
        }

        inUse = true;
        console.log("Setting colors to red...");
        sense.setPixels(colorRed);

        setTimeout(() => {
            console.log("Setting colors back to white...");
            sense.setPixels(colorWhite);
            inUse = false;
        }, 250);
    });
};
